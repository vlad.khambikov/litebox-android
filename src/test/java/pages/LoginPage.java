package pages;

import basic.LiteboxMobileDriver;
import basic.Log;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.By;

public class LoginPage extends AnyCashBoxPage {

    private By loginPageLoginInput = By.id(String.format("%s%s", driver.PACK, ":id/loginText"));
    private By loginPagePasswordInput = By.id(String.format("%s%s", driver.PACK, ":id/etPassword"));
    private By loginPageEnterButton = By.id(String.format("%s%s", driver.PACK, ":id/enter_button"));
    private By user = By.id("android:id/text1");


    public LoginPage(LiteboxMobileDriver driver) {
        super(driver);
    }


    public OpenSessionPage login(String liteboxLogin, String liteboxPassword) {
        Log.info("Вводим логин");

        int x = el(loginPageLoginInput).getXRight() - 2;
        int y = el(loginPageLoginInput).getYMiddle();
        new TouchAction(driver.getWebDriver()).press(x, y).perform();
        if (waitIsVisibleElement(user, 2, "окно выбора пользователя")) {
            el(user).click();
        }
        el(loginPageLoginInput).sendKeys(liteboxLogin);
        el(loginPagePasswordInput).sendKeys(liteboxPassword);
        el(loginPageEnterButton).click();
        return new SaveCashbox(driver).saveSetting();
    }
}
