package pages;

import Data.Scrolling;
import basic.LiteboxMobileDriver;
import basic.Log;
import io.appium.java_client.TouchAction;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AnyCashBoxPage extends PageObject {

    private By errorTitle = By.id(String.format("%s%s", driver.PACK, ":id/headerView"));
    private By errorMessage = By.id(String.format("%s%s", driver.PACK, ":id/descriptionView"));
    private By errorCloseButtom = By.id(String.format("%s%s", driver.PACK, ":id/acceptView"));
    private WebDriverWait wait;


    public AnyCashBoxPage(LiteboxMobileDriver driver) {
        super(driver);
        //pack = driver.getWebDriver().getCurrentPackage();
    }


    protected String errorPopup() {
        String errorMess = "No";
        wait = new WebDriverWait(driver.getWebDriver(), 1);
        long beginTime = System.currentTimeMillis();
        try {
            wait.until(d -> d.findElement(errorTitle).getText().equals("Ошибка"));
            Log.info(String.format(" ...ожидал появления окна ошибки: %s мс", (System.currentTimeMillis() - beginTime)));
            errorMess = el(errorMessage).getText();
            el(errorCloseButtom).click();
            return errorMess;
        } catch (TimeoutException ex) {
            return errorMess;
        }
    }

    public boolean waitIsVisibleElement(By by, int seconds, String messeage) {
        wait = new WebDriverWait(driver.getWebDriver(), seconds);
        long beginTime = System.currentTimeMillis();
        try {
            wait.until(d -> d.findElement(by).isEnabled());
            Log.info(String.format(" ...ожидал появления %s: %s мс", messeage, (System.currentTimeMillis() - beginTime)));
            return true;
        } catch (TimeoutException ex) {
            Log.info(String.format("В течении %d секунд не появилось %s", seconds, messeage));
            return false;
        }
    }

    public AnyCashBoxPage scroll(By by, Scrolling scrolling) {
        Point pointSt = el(by).getLocation();
        Dimension size = el(by).getSize();
        int xLeft = pointSt.getX() + size.getWidth() / 20;
        int xMidlle = pointSt.getX() + size.getWidth() / 2;
        int xRight = pointSt.getX() + size.getWidth() * 4 / 5;
        int yUp = pointSt.getY() + size.getHeight() / 20;
        int yMiddle = pointSt.getY() + size.getHeight() / 2;
        int yDown = pointSt.getY() + size.getHeight() * 4 / 5;
        TouchAction action = new TouchAction(driver.getWebDriver());
        switch (scrolling) {
            case UP:
                action.press(xMidlle, yDown);
                action.waitAction(Duration.ofMillis(700));
                action.moveTo(xMidlle, yUp);
                break;
            case DOWN:
                action.press(xMidlle, yUp);
                action.waitAction(Duration.ofMillis(700));
                action.moveTo(xMidlle, yDown);
                break;
            case LEFT:
                action.press(xRight, yMiddle);
                action.waitAction(Duration.ofMillis(700));
                action.moveTo(xLeft, yMiddle);
                break;
            case RIGHT:
                action.press(xLeft, yMiddle);
                action.waitAction(Duration.ofMillis(700));
                action.moveTo(xRight, yMiddle);
                break;
        }
        action.release();
        action.perform();
        return this;
    }

    public AnyCashBoxPage visibleDescContainer(){
        if(waitIsVisibleElement(By.id("com.android.packageinstaller:id/desc_container"), 2, "разрешить приложению")){
            if(el(By.id("com.android.packageinstaller:id/permission_message")).getText().contains("местоположении")){
                Log.info("Запрещаем кассе доступ к местоположению устройства");
                el(By.id("com.android.packageinstaller:id/permission_deny_button")).click();
            }
        }
        return this;
    }


/*
    public AnyCashBoxPage moveTo(int xst, int yst, int xfin, int yfin, int count) {

        int xdef = (xfin - xst) / count;
        int ydef = (yfin - yst) / count;
        TouchAction action = new TouchAction(driver.getWebDriver());
        action.press(xst, yst);
        boolean wh = false;
        if (xdef == 0 && ydef != 0) {
            wh = ydef > 0 ?
                    yst + ydef < yfin
                    : yst + ydef > yfin;
        } else if (ydef == 0 && xdef != 0) {
            wh = xdef > 0 ?
                    xst + xdef < xfin
                    : xst + xdef > xfin;
        } else if (xdef != 0 && ydef != 0) {
            wh = (ydef > 0 ? yst + ydef < yfin : yst + ydef > yfin) || (xdef > 0 ? xst + xdef < xfin : xst + xdef > xfin);
        } else {
            Assert.fail("ошибка xdef и ydef равны 0");
        }
        while (wh) {
            xst += xdef;
            yst += ydef;
            action.waitAction(Duration.ofMillis(1000/count));
            action.moveTo(xst, yst);
            if (xdef == 0 && ydef != 0) {
                wh = ydef > 0 ?
                        yst + ydef < yfin
                        : yst + ydef > yfin;
            } else if (ydef == 0 && xdef != 0) {
                wh = xdef > 0 ?
                        xst + xdef < xfin
                        : xst + xdef > xfin;
            } else if (xdef != 0 && ydef != 0) {
                wh = (ydef > 0 ? yst + ydef < yfin : yst + ydef > yfin) || (xdef > 0 ? xst + xdef < xfin : xst + xdef > xfin);
            } else {
                Assert.fail("ошибка xdef и ydef равны 0");
            }
        }
        action.release();
        action.perform();
        return this;
    }*/
}
