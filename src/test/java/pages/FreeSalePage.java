package pages;

import basic.LiteboxMobileDriver;
import basic.Log;
import org.openqa.selenium.By;

public class FreeSalePage extends CashboxMainPage {

    private By clearAcButton = By.id(String.format("%s%s", driver.PACK, ":id/clear"));
    private By backSpaceButton = By.id(String.format("%s%s", driver.PACK, ":id/backSpace"));
    private By divideButton = By.id(String.format("%s%s", driver.PACK, ":id/divide"));
    private By multiplyButton = By.id(String.format("%s%s", driver.PACK, ":id/multiply"));
    private By digitOneButton = By.id(String.format("%s%s", driver.PACK, ":id/digitOne"));
    private By digitTwoButton = By.id(String.format("%s%s", driver.PACK, ":id/digitTwo"));
    private By digitThreeButton = By.id(String.format("%s%s", driver.PACK, ":id/digitThree"));
    private By plusButton = By.id(String.format("%s%s", driver.PACK, ":id/plus"));
    private By digitFourButton = By.id(String.format("%s%s", driver.PACK, ":id/digitFour"));
    private By digitFiveButton = By.id(String.format("%s%s", driver.PACK, ":id/digitFive"));
    private By digitSixButton = By.id(String.format("%s%s", driver.PACK, ":id/digitSix"));
    private By minusButton = By.id(String.format("%s%s", driver.PACK, ":id/minus"));
    private By digitSevenButton = By.id(String.format("%s%s", driver.PACK, ":id/digitSeven"));
    private By digitEightButton = By.id(String.format("%s%s", driver.PACK, ":id/digitEight"));
    private By digitNineButton = By.id(String.format("%s%s", driver.PACK, ":id/digitNine"));
    private By equallyButton = By.id(String.format("%s%s", driver.PACK, ":id/equally"));
    private By delemiterButton = By.id(String.format("%s%s", driver.PACK, ":id/delemiter"));
    private By digitZeroButton = By.id(String.format("%s%s", driver.PACK, ":id/digitZero"));
    private By enterButton = By.id(String.format("%s%s", driver.PACK, ":id/enter"));

    private By getValue = By.id(String.format("%s%s", driver.PACK, ":id/internalEditField"));

    public FreeSalePage(LiteboxMobileDriver driver) {
        super(driver);
    }

    public CashboxMainPage enterAnyNumber(String cost) {
        char[] chars = cost.toCharArray();
        for (char ch : chars) {
            switch (ch) {
                case '0':
                    el(digitZeroButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '1':
                    el(digitOneButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '2':
                    el(digitTwoButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '3':
                    el(digitThreeButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '4':
                    el(digitFourButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '5':
                    el(digitFiveButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '6':
                    el(digitSixButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '7':
                    el(digitSevenButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '8':
                    el(digitEightButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '9':
                    el(digitNineButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case ',':
                case '.':
                    el(delemiterButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case 'a':
                    el(clearAcButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case 'd':
                    el(backSpaceButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '/':
                    el(divideButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '*':
                    el(multiplyButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '+':
                    el(plusButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '-':
                    el(minusButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case '=':
                    el(equallyButton).click();
                    Log.info(String.format("Значение в поле вода: %s", el(getValue).getText()));
                    break;
                case 'e':
                    el(enterButton).click();
                    return new BaskePage(driver);
            }
        }
        return new CashboxMainPage(driver);
    }
}
