package pages;

import basic.LiteboxMobileDriver;
import basic.Log;
import org.openqa.selenium.By;

public class CashboxMainPage extends AnyCashBoxPage {

    //private By closeSessionButton = By.id(String.format("%s%s", driver.PACK, ":id/close_button"));
    private By mainMenuButton = By.id(String.format("%s%s", driver.PACK, ":id/main_menu"));
    private By secondaryPageMainMenuButton = By.id(String.format("%s%s", driver.PACK, ":id/payButtonValue"));
    private By toolbarTitle = By.id(String.format("%s%s", driver.PACK, ":id/mainToolbarTitle"));
    private By favoriteButton = By.id(String.format("%s%s", driver.PACK, ":id/favorite"));
    private By freeSaleButton = By.id(String.format("%s%s", driver.PACK, ":id/free_sale"));
    private By basketButton = By.id(String.format("%s%s", driver.PACK, ":id/basket_button"));


    public CashboxMainPage(LiteboxMobileDriver driver) {
        super(driver);
    }

    public MainMenuPage openMainMenu() {
        Log.info("Открываем Главное меню");
        el(mainMenuButton).click();
        return new MainMenuPage(driver);
    }

    public CashboxMainPage exit() {
        Log.info("Выход из кассы");
        openMainMenu().exit().exitSession();
        return this;
    }

    public FavoritePage gotoFavorite() {
        if (el(toolbarTitle).getText().equals("Избранное")) {
            Log.info("Уже в \"Избранное\"");
        } else {
            Log.info("Переходим в \"Избранное\"");
            el(favoriteButton).click();
        }
        return new FavoritePage(driver);
    }

    public FreeSalePage gotoFreeSale() {
        if (el(toolbarTitle).getText().equals("Своб. прод.")) {
            Log.info("Уже в \"Свободная продажа\"");
        } else {
            Log.info("Переходим в \"Свободная продажа\"");
            el(freeSaleButton).click();
        }
        return new FreeSalePage(driver);
    }

    public BaskePage gotoBaskePage() {
        if (el(toolbarTitle).getText().equals("Чек")) {
            Log.info("Уже в \"Чек\"");
        } else {
            Log.info("Переходим в \"Чек\"");
            el(basketButton).click();
        }
        return new BaskePage(driver);
    }


}
