package pages;

import basic.LiteboxMobileDriver;
import basic.Log;
import io.appium.java_client.MobileElement;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static basic.Utils.random;

public class SaveCashbox extends AnyCashBoxPage {

    private By selectedShopSpinnerButton = By.id(String.format("%s%s", driver.PACK, ":id/shopSpinner"));
    private By selectedCashDeskSpinnerButton = By.id(String.format("%s%s", driver.PACK, ":id/cashDeskSpinner"));
    private By saveSettingCashbox = By.id(String.format("%s%s", driver.PACK, ":id/save_button"));
    private By shopsSpinnerSelected = By.id("android:id/text1");
    private By shopDefault = By.xpath(String.format("//android.widget.Spinner[@resource-id='%s:id/shopSpinner']/android.widget.TextView[@resource-id='android:id/text1']", driver.PACK)); //    /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.Spinner[1]/android.widget.TextView");
    private By cashboxDefault = By.xpath(String.format("//android.widget.Spinner[@resource-id='%s:id/cashDeskSpinner']/android.widget.TextView[@resource-id='android:id/text1']", driver.PACK));
    private By settingCashbox = By.id(String.format("%s%s", driver.PACK,  ":id/header")); //:id/image_sett"));


    private WebDriverWait wait;


    public SaveCashbox(LiteboxMobileDriver driver) {
        super(driver);
    }

    public OpenSessionPage saveSetting() {
        Assert.assertTrue("Не появилось окно открытия настроек кассы", waitIsVisibleSettingCashbox(10));
        String shopName = el(shopDefault).getText();
        String cashboxName;
        if (shopName.isEmpty()) {
            el(selectedShopSpinnerButton).click();
            List<MobileElement> elements = getShops();
            boolean cashBoxSelect = false;
            List<String> selectShopName = new ArrayList<>();
            while (elements.size() > 0) {
                MobileElement element = elements.get(0);
                selectShopName.add(element.getText());
                element.click();
                if (!errorPopup().equals("No")) {
                    el(selectedShopSpinnerButton).click();
                    elements = getShops(selectShopName);
                    continue;
                }
                driver.waitUnveil();
                cashboxName = el(cashboxDefault).getText();
                if (!cashboxName.isEmpty()) {
                    Log.info(String.format("Выбрали кассу: %s", cashboxName));
                    cashBoxSelect = true;
                    break;
                } else {
                    el(selectedShopSpinnerButton).click();
                    elements = getShops(selectShopName);
                    continue;
                }
            }
            if (!cashBoxSelect) {
                MobileElement element = elements.get(random.nextInt(elements.size()));
                shopName = element.getText();
                List<String> shopsWithoutCashbox = new ArrayList<>();
                while (!errorPopup().equals("No")) {
                    shopsWithoutCashbox.add(shopName);
                    Assert.assertTrue("Нет ни одного магазина c доступными кассами", elements.size() > 0);
                    el(selectedShopSpinnerButton).click();
                    elements = getShops(shopsWithoutCashbox);
                    element = elements.get(random.nextInt(elements.size()));
                    shopName = element.getText();
                    element.click();
                }
            }
        } else {
            Log.info(String.format("Выбрали магазин: %s", shopName));
            driver.waitUnveil();
            cashboxName = el(cashboxDefault).getText();
            if (cashboxName.isEmpty()) {

            } else {
                Log.info(String.format("Выбрали кассу: %s", cashboxName));
            }
        }
        return saveSettingCashbox();

    }

    public boolean waitIsVisibleSettingCashbox(int seconds) {
        wait = new WebDriverWait(driver.getWebDriver(), seconds);
        long beginTime = System.currentTimeMillis();
        try {
            wait.until(d -> d.findElement(settingCashbox).getText().equals("Настроить кассу"));
            Log.info(String.format(" ...ожидал появления окна настройки касс: %s мс", (System.currentTimeMillis() - beginTime)));
            return true;
        } catch (TimeoutException ex) {
            Log.info("В течении 10 секунд не появилось окно настройки кассы");
            return false;
        }
    }


    public OpenSessionPage saveSettingCashbox() {
        Log.info("Сохраняем настройки кассы");
        el(saveSettingCashbox).click();
        return new OpenSessionPage(driver);
    }

    public List<MobileElement> getShops() {
        List<MobileElement> elements = driver.findElements(shopsSpinnerSelected)
                .stream()
                .filter((s) -> !s.getText().equals(""))
                .collect(Collectors.toList());
        Assert.assertTrue("Нет ни одного магазина c доступными кассами", elements.size() > 0);
        return elements;
    }

    public List<MobileElement> getShops(List<String> list) {
        List<MobileElement> elements = driver.findElements(shopsSpinnerSelected)
                .stream()
                .filter((s) -> !s.getText().equals("") && !list.contains(s.getText()))
                .collect(Collectors.toList());
        Assert.assertTrue("Нет ни одного магазина c доступными кассами", elements.size() > 0);
        return elements;
    }

}
