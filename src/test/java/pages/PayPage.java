package pages;

import basic.LiteboxMobileDriver;
import basic.Log;
import io.appium.java_client.MobileElement;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.List;

import static basic.Utils.random;
import static basic.Utils.round;

public class PayPage extends BaskePage {

    private By gotoBaskePageButton = By.id(String.format("%s%s", driver.PACK, ":id/titleFrame"));
    private By paymenIncomeText = By.id(String.format("%s%s", driver.PACK, ":id/paymentIncome"));
    private By toPayText = By.id(String.format("%s%s", driver.PACK, ":id/total"));
    private By paymentPriceSendText = By.id(String.format("%s%s", driver.PACK, ":id/paymentPrice"));
    private By addNewPaymentButton = By.id(String.format("%s%s", driver.PACK, ":id/addNewPayment"));
    private By getTypePayment = By.id(String.format("%s%s", driver.PACK, ":id/typeText"));
    private By switchPaymentCash = By.xpath(String.format("//android.widget.RelativeLayout[@resource-id='%s:id/paymentCash']/android.widget.RelativeLayout/android.widget.Switch", driver.PACK));
    private By switchPaymentCard = By.xpath(String.format("//android.widget.RelativeLayout[@resource-id='%s:id/paymentCard']/android.widget.RelativeLayout/android.widget.Switch", driver.PACK));
    private By addPaymentPopupCashButton = By.id(String.format("%s:id/paymentCash", driver.PACK));
    private By addPaymentPopupCardButton = By.id(String.format("%s:id/paymentCard", driver.PACK));
    private By addPaymentPopupAcceptButton = By.id(String.format("%s:id/acceptView", driver.PACK));
    private By payButton = By.id(String.format("%s:id/payButton", driver.PACK));
    private By deliveryText = By.id(String.format("%s::id/cashRest", driver.PACK));



    public PayPage(LiteboxMobileDriver driver) {
        super(driver);
    }

    public BaskePage gotoBaskePage() {
        Log.info("Возврящаемся в окно чека");
        el(gotoBaskePageButton).click();
        return new BaskePage(driver);
    }

    public BaskePage pay () {
        Log.info("Нажимаем кнопку оплатить");
        if(waitIsVisibleElement(deliveryText, 1, "")){
            Log.info(String.format("Сдача %s", el(deliveryText).getText()));
        }
        el(payButton).click();
        return new BaskePage(driver);
    }

    public PayPage beforeClickPay(float cost, boolean nextPayment, float... perc) {
        String type = el(getTypePayment).getText();
        if (nextPayment && perc.length > 0) {
            if (random.nextBoolean()) {
                el(getTypePayment).click();
                List<MobileElement> elements = driver.findElements(getTypePayment);
                Assert.assertTrue("нет другого типа метода оплаты", elements.size() > 1);
                for (MobileElement element : elements) {
                    if (!element.getText().equals(type)) {
                        Log.info(String.format(""));
                        type = element.getText();
                        element.click();
                        break;
                    }
                }
            }
            float first = round(cost * perc[0] / 100, 2);
            float next = round(cost - first, 2);
            el(paymentPriceSendText).sendKeys(String.valueOf(first));
            el(addNewPaymentButton).click();
            String text;
            switch (type) {
                case "Наличные":
                    el(switchPaymentCash).click();
                    el(addPaymentPopupAcceptButton).click();
                    break;
                case "Банковская карта":
                    el(switchPaymentCard).click();
                    el(addPaymentPopupAcceptButton).click();
            }
            text = errorPopup();
            Assert.assertFalse("Не появилось окно ошибки что данный тип оплаты уже применен", text.equals("No"));
            Log.info(String.format("Появилось окно ошибки %s. Все верно", text));
            el(addNewPaymentButton).click();
            switch (type) {
                case "Наличные":
                    el(addPaymentPopupCardButton).click();
                    break;
                case "Банковская карта":
                    el(addPaymentPopupCashButton).click();
                    break;
            }
            text = errorPopup();
            Assert.assertTrue(String.format("Появилось окно ошибки %s. Все верно", text), text.equals("No"));
            driver.findElements(paymentPriceSendText).get(1).sendKeys(String.valueOf(next));
        } else {
            if (random.nextBoolean()) {
                el(getTypePayment).click();
                List<MobileElement> elements = driver.findElements(getTypePayment);
                Assert.assertTrue("нет другого типа метода оплаты", elements.size() > 1);
                for (MobileElement element : elements) {
                    if (!element.getText().equals(type)) {
                        Log.info(String.format(""));
                        type = element.getText();
                        element.click();
                        break;
                    }
                }
            }
            el(paymentPriceSendText).sendKeys(String.valueOf(cost));
        }
        return this;
    }

}
