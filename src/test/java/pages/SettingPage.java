package pages;

import Data.Scrolling;
import basic.LiteboxMobileDriver;
import basic.Log;
import io.appium.java_client.MobileElement;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.List;

public class SettingPage extends CashboxMainPage {

    private By anyTab = By.xpath(String.format("//android.widget.TextView[@resource-id='%s:id/psts_tab_title'] ", driver.PACK));//"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.TextView");

    private By titleBarSetting = By.id(String.format("%s%s", driver.PACK, ":id/preferences_pager_title_indicator"));
    private By saveSettingButton = By.id(String.format("%s%s", driver.PACK, ":id/save_changes_button"));
    private By returnDefaultsSettingButton = By.id(String.format("%s%s", driver.PACK, ":id/return_to_defaults_button"));
    private By exchangeProgress = By.id(String.format("%s%s", driver.PACK, ":id/exchange_progress"));
    private By moveUpButton = By.xpath("//android.widget.ImageButton[@content-desc=\"Перейти вверх\"]");

    private By exchangeButton = By.xpath(String.format("//android.widget.ListView[@resource-id='%s:id/preferencesListView']//android.widget.Button[@resource-id='%s:id/settingsButton']", driver.PACK, driver.PACK));//"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button");
    private By closeSettingForTableButton = By.xpath(String.format("//android.widget.LinearLayout[@resource-id='%s:id/footer']//android.widget.RelativeLayout[@resource-id='%s:id/close_button']", driver.PACK, driver.PACK));
    private By closeConfirmPopup = By.id(String.format("%s:id/descriptionView", driver.PACK));
    private By closeConfirmAcceptButton = By.xpath(String.format("//android.widget.LinearLayout[@resource-id='%s:id/buttonLayout']/android.widget.TextView[@resource-id='%s:id/acceptView']", driver.PACK, driver.PACK));

    public SettingPage(LiteboxMobileDriver driver) {
        super(driver);
    }

    public SettingPage gotoCommon() {
        scroll(titleBarSetting, Scrolling.RIGHT);
        List<MobileElement> elements = driver.findElements(anyTab);
        int i = 0;
        while (i < elements.size()) {
            String text = elements.get(i).getText();
            if (text.contains("Общие")) {
                switch (elements.get(i).getAttribute("selected")) {
                    case "true":
                        Log.info("Уже на вкладке \"Общие параметры\"");
                        return this;
                    case "false":
                        Log.info("Переходим на вкладку \"Общие параметры\"");
                        elements.get(i).click();
                        return this;
                }
            } else {
                i++;
                if (i == elements.size()) {
                    elements.get(i - 1).click();
                    driver.waitUnveil();
                    elements = driver.findElements(anyTab);
                    i = 0;
                }
            }
        }
        Assert.fail("Нет вкладки \"Общие параметры\"");
        return this;
    }

    public SettingPage gotoExchange() {
        scroll(titleBarSetting, Scrolling.RIGHT);
        List<MobileElement> elements = driver.findElements(anyTab);
        int i = 0;
        while (i < elements.size()) {
            String text = elements.get(i).getText();
            if (text.toLowerCase().contains("обмен")) {
                switch (elements.get(i).getAttribute("selected")) {
                    case "true":
                        Log.info("Уже на вкладке \"Параметры обмена\"");
                        return this;
                    case "false":
                        Log.info("Переходим на вкладку \"Параметры обмена\"");
                        elements.get(i).click();
                        return this;
                }
            } else {
                i++;
                if (i == elements.size()) {
                    elements.get(i - 1).click();
                    driver.waitUnveil();
                    elements = driver.findElements(anyTab);
                    i = 0;
                }
            }
        }
        Assert.fail("Нет вкладки \"Параметры обмена\"");
        return this;
    }

    public SettingPage gotoNotice() {
        scroll(titleBarSetting, Scrolling.RIGHT);
        List<MobileElement> elements = driver.findElements(anyTab);
        int i = 0;
        while (i < elements.size()) {
            String text = elements.get(i).getText();
            if (text.equals("Уведомления")) {
                switch (elements.get(i).getAttribute("selected")) {
                    case "true":
                        Log.info("Уже на вкладке \"Уведомления\"");
                        return this;
                    case "false":
                        Log.info("Переходим на вкладку \"Уведомления\"");
                        elements.get(i).click();
                        return this;
                }
            } else {
                i++;
                if (i == elements.size()) {
                    elements.get(i - 1).click();
                    driver.waitUnveil();
                    elements = driver.findElements(anyTab);
                    i = 0;
                }
            }
        }
        Assert.fail("Нет вкладки \"Уведомления\"");
        return this;
    }

    public SettingPage gotoEquipment() {
        scroll(titleBarSetting, Scrolling.RIGHT);
        List<MobileElement> elements = driver.findElements(anyTab);
        int i = 0;
        while (i < elements.size()) {
            String text = elements.get(i).getText();
            if (text.equals("Оборудование")) {
                switch (elements.get(i).getAttribute("selected")) {
                    case "true":
                        Log.info("Уже на вкладке \"Оборудование\"");
                        return this;
                    case "false":
                        Log.info("Переходим на вкладку \"Оборудование\"");
                        elements.get(i).click();
                        return this;
                }
            } else {
                i++;
                if (i == elements.size()) {
                    elements.get(i - 1).click();
                    driver.waitUnveil();
                    elements = driver.findElements(anyTab);
                    i = 0;
                }
            }
        }
        Assert.fail("Нет вкладки \"Оборудование\"");
        return this;
    }

    public SettingPage gotoEGAIS() {
        scroll(titleBarSetting, Scrolling.RIGHT);
        List<MobileElement> elements = driver.findElements(anyTab);
        int i = 0;
        while (i < elements.size()) {
            String text = elements.get(i).getText();
            if (text.equals("ЕГАИС")) {
                switch (elements.get(i).getAttribute("selected")) {
                    case "true":
                        Log.info("Уже на вкладке \"ЕГАИС\"");
                        return this;
                    case "false":
                        Log.info("Переходим на вкладку \"ЕГАИС\"");
                        elements.get(i).click();
                        return this;
                }
            } else {
                i++;
                if (i == elements.size()) {
                    elements.get(i - 1).click();
                    driver.waitUnveil();
                    elements = driver.findElements(anyTab);
                    i = 0;
                }
            }
        }
        Assert.fail("Нет вкладки \"ЕГАИС\"");
        return this;
    }

    public CashboxMainPage completeExchange() {
        gotoExchange();
        if (!waitIsVisibleElement(exchangeProgress, 1, "Полный обмен")) {
            el(exchangeButton).click();
        }
        driver.waitUntilInvisibleIfExists(exchangeProgress);
        Log.info("Совершили полный обмен");
        return closeSetting();
    }

    public CashboxMainPage closeSetting(){
        String deviceModel = driver.getDeviceModel();
        if (driver.GETALLTABLE().contains(deviceModel)) {
            el(closeSettingForTableButton).click();
            int i = 0;
            while (waitIsVisibleElement(closeConfirmPopup, 1, "Закрыть без сохранения настроек?")) {
                Log.info(String.format("Закрываем окно сохранения настроек без сохранения, попытка: %d", ++i));
                el(closeConfirmAcceptButton).click();
            }
        }else if (waitIsVisibleElement(moveUpButton, 1, "Назад")){
            el(moveUpButton).click();
        }
        return new CashboxMainPage(driver);
    }


}
