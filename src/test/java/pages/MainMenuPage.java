package pages;

import Data.Scrolling;
import basic.LiteboxMobileDriver;
import org.openqa.selenium.By;


public class MainMenuPage extends AnyCashBoxPage {

    private By firstItemButton = By.id(String.format("%s%s", driver.PACK, ":id/first_item_layout")); //заблокировать касу, удаленный доступ
    private By secondaryItemButton = By.id(String.format("%s%s", driver.PACK, ":id/second_item_layout")); //справочники, информация о кассе
    private By thirdItemButton = By.id(String.format("%s%s", driver.PACK, ":id/third_item_layout")); //кассовые операции, обратная связь
    private By fourthItemButton = By.id(String.format("%s%s", driver.PACK, ":id/fourth_item_layout")); //кассовые отчеты, выход
    private By fifthItemButton = By.id(String.format("%s%s", driver.PACK, ":id/fifth_item_layout")); //setting holdings, feedback
    private By sixthItemButton = By.id(String.format("%s%s", driver.PACK, ":id/sixth_item_layout")); //exit, trading reports
    private By firstItemDropMenuButton = By.id(String.format("%s%s", driver.PACK, ":id/menu_drop_item1")); //setting
    private By secondItemDropMenuButton = By.id(String.format("%s%s", driver.PACK, ":id/menu_drop_item2")); //cash block

    private By menuTabLayout = By.id(String.format("%s%s", driver.PACK, ":id/menuTabLayout"));
    private By menuPageForGetSize = By.id(String.format("%s%s", driver.PACK, ":id/side_menu"));

    private By settingElement = By.xpath(String.format("//android.support.v4.view.ViewPager[@resource-id='%s:id/menuViewPager']//android.widget.TextView[contains(@resource-id,':id/text')][@text='Настройки']/..", driver.PACK)); //"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView");
    private By exitElement = By.xpath(String.format("//android.support.v4.view.ViewPager[@resource-id='%s:id/menuViewPager']//android.widget.TextView[contains(@resource-id,':id/text')][@text='ВЫХОД']/..", driver.PACK)); //"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView");

    public MainMenuPage(LiteboxMobileDriver driver) {
        super(driver);
    }

    private MainMenuPage gotoFirstPage() {
        scroll(menuPageForGetSize, Scrolling.RIGHT);
        return this;
    }

    private MainMenuPage gotoSecondPage() {
        scroll(menuPageForGetSize, Scrolling.LEFT);
        return this;
    }

    public ExitPage exit() {
        if (waitIsVisibleElement(exitElement, 2, "Настройки")) {
            el(exitElement).click();
        } else {
            gotoSecondPage();
            el(exitElement).click();
        }
        return new ExitPage(driver);
    }

    public SettingPage setting() {
        if (waitIsVisibleElement(settingElement, 2, "Настройки")) {
            el(settingElement).click();
        } else {
            gotoSecondPage();
            el(settingElement).click();
        }
        return new SettingPage(driver);
    }

    /*
    private boolean seachAndClick(String text){
        List<MobileElement> elements = driver.findElements(textMaynMeny);
        for(MobileElement element: elements){
            if (text.equals(element.getText())){
                element.click();
                driver.waitUnveil();
                return true;
            }
        }
        return false;
    }*/
}
