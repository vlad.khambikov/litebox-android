package pages;

import basic.Element;
import basic.LiteboxMobileDriver;
import org.openqa.selenium.By;

public class PageObject {
    protected LiteboxMobileDriver driver;

    public PageObject(LiteboxMobileDriver driver) {
        this.driver = driver;
    }

    protected Element el(By by) {
        Element element = driver.find(by);
        if (element == null) {
            driver.takeScreenshot();
            throw new RuntimeException("Не могу найти элемент "+ by);
        }
        return element;
    }
}
