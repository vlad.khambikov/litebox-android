package pages;

import basic.LiteboxMobileDriver;
import basic.Log;
import org.junit.Assert;
import org.openqa.selenium.By;

public class OpenSessionPage extends AnyCashBoxPage {

    private By openSessionTittle = By.id(String.format("%s:id/custom_title", driver.PACK));
    private By onlyOpenSession = By.id(String.format("%s%s", driver.PACK, ":id/only_open_session"));
    private By printReportAndOpenSession = By.id(String.format("%s%s", driver.PACK, ":id/print_report_and_open"));
    private By closeSessionButton = By.id(String.format("%s%s", driver.PACK, ":id/only_close_session"));
    private By openSessionPageDepositMoneyInCash = By.id(String.format("%s:id/addCashMoneyContainer", driver.PACK));
    private By openSessoinPageSubType = By.id(String.format("%s:id/subTypeSpinner", driver.PACK));
    private By openSessoinPageSubject = By.id(String.format("%s:id/subjectEditText", driver.PACK));
    private By openSessoinPageSum = By.id(String.format("%s:id/sumEditView", driver.PACK));
    private By openSessoinPageDpcumen = By.id(String.format("%s:id/documentEditView", driver.PACK));
    private By openSessoinPageComment = By.id(String.format("%s:id/commentEditView", driver.PACK));
    private By openSessoinDepositButton = By.id(String.format("%s:id/:id/bottomButton", driver.PACK));
    private By openSessoinPageGetCashOfHandText = By.xpath("//android.widget.TableLayout[contains(@resource-id,':id/tableCashInfo')]//android.widget.TextView[@text='НАЛИЧНЫЕ В КАССЕ:']/../android.widget.TextView[contains(@resource-id,':id/value')]");//By.id(String.format("%s:id/:id/bottomButton", driver.PACK));



    private String openSessionPageSelectSubType = "//android.widget.CheckedTextView[@text='%s']";

    public OpenSessionPage(LiteboxMobileDriver driver) {
        super(driver);
        visibleDescContainer();
        visibleDescContainer();
        el(openSessionTittle).waitingForAppearanceOfAttributeWithValue("text", "Открытие смены", 4);
    }

    public CashboxMainPage onlyOpenSession() {
        Log.info("Не печатаем чек открытия смены, а просто открываем смену");
        el(onlyOpenSession).click();
        return new CashboxMainPage(driver);
    }

    public CashboxMainPage printReportAndOpenSession() {
        Log.info("Печатаем чек открытия смены и открываем смену");
        el(printReportAndOpenSession).click();
        return new CashboxMainPage(driver);
    }

    public CashboxMainPage onlycloseSession() {
        Log.info("Не открываем смену");
        el(closeSessionButton).click();
        return new CashboxMainPage(driver);
    }

    public String getCashOfHand(){
        String cashOfHand = el(openSessoinPageGetCashOfHandText).getText();
        Log.info(String.format("Наличные в кассе: %s", cashOfHand));
        return cashOfHand;
    }

    public OpenSessionPage depositMoneyInCash(String type, String fromWhom, String ammount, String... otherPatams) {
        Log.info("Вносим деньги в кассу");
        el(openSessionPageDepositMoneyInCash).click();
        Assert.assertTrue("Не расскрылось подменю внести деньги в кассу", waitIsVisibleElement(openSessoinPageSubType, 2, "внести деньги в кассу"));
        el(openSessoinPageSubType).click();
        el(By.xpath(String.format(openSessionPageSelectSubType, type))).click();
        el(openSessoinPageSubject).sendKeys(fromWhom);
        scroll()
        el(openSessoinPageSum).sendKeys(ammount);
        if (otherPatams.length > 0){
            if(otherPatams.length == 1 ){
                Log.info(String.format("Вводим название документа: %s", otherPatams[0]));
                el(openSessoinPageDpcumen).sendKeys(otherPatams[0]);
            }else {
                Log.info(String.format("Вводим название документа: %s", otherPatams[0]));
                el(openSessoinPageDpcumen).sendKeys(otherPatams[0]);
                Log.info(String.format("Вводим комментарий к внесению: %s", otherPatams[1]));
                el(openSessoinPageComment).sendKeys(otherPatams[1]);
            }
        }
        el(openSessoinDepositButton).click();
        return this;
    }


}
