package pages;

import basic.LiteboxMobileDriver;
import basic.Log;
import org.openqa.selenium.By;

public class BaskePage extends CashboxMainPage {

    private By seachInput = By.id(String.format("%s%s", driver.PACK, ":id/search"));
    private By payButton = By.id(String.format("%s%s", driver.PACK, ":id/payButton"));
    private By payPanelButton = By.id(String.format("%s%s", driver.PACK, ":id/payButtonPanel"));
    private By payValueText = By.id(String.format("%s%s", driver.PACK, ":id/payButtonValue"));
    private By waresInBaske = By.id(String.format("%s%s", driver.PACK, ":id/goodInfoContainer"));
    private By paymentCashButton = By.id(String.format("%s%s", driver.PACK, ":id/paymentCash"));
    private By paymentCardButton = By.id(String.format("%s%s", driver.PACK, ":id/paymentCard"));
    private By basketResultText = By.id(String.format("%s:id/basketResultTotal", driver.PACK));


    public BaskePage(LiteboxMobileDriver driver) {
        super(driver);
    }

    public BaskePage seach(String text) {
        Log.info(String.format("Поиск товара: %s", text));
        el(seachInput).sendKeys(text);
        return this;
    }

    public BaskePage pay(String wayPayment, float cost, boolean nextPayment, float ... perc) {
        if (el(payValueText).getText().equals("0,00")) {
            Log.info("Нечего оплачивать, выберете товар");
            return this;
        } else {
            el(payButton).click();
            switch (wayPayment) {
                case "Cash":
                    Log.info("Выбираем способ оплаты: Наличные");
                    el(paymentCashButton).click();
                    break;
                case "Card":
                    Log.info("Выбираем способ оплаты: Банковской картой");
                    el(paymentCardButton).click();
                    break;
            }
            return new PayPage(driver).beforeClickPay(cost, nextPayment, perc).pay();
        }
    }

    public int getCountWaresInBaske() {
        return driver.findElements(waresInBaske).size();
    }

    public float getTotal(){
        float total = Float.parseFloat(el(basketResultText).getText().replace(",","."));
        Log.info(String.format("Итого: %f", total));
        return total;
    }
}
