package pages;

import basic.LiteboxMobileDriver;
import org.openqa.selenium.By;

public class FavoritePage extends CashboxMainPage {

    private By anyCellFavorite = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v4.view.ViewPager/android.widget.FrameLayout/android.widget.GridView/android.widget.FrameLayout/android.widget.LinearLayout");

    private By favoritePage = By.id(String.format("%s%s", driver.PACK, ":id/favorites_fragment_root"));

    public FavoritePage(LiteboxMobileDriver driver) {
        super(driver);
    }

    public int getCountWaresFavoriteOnePage(){
        return driver.findElements(anyCellFavorite).size();
    }




}
