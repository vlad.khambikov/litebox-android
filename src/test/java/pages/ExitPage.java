package pages;

import basic.LiteboxMobileDriver;
import basic.Log;
import org.openqa.selenium.By;

public class ExitPage extends AnyCashBoxPage {

    private By closeSessionButton = By.id(String.format("%s%s", driver.PACK, ":id/only_close_session"));
    private By cancelCloseSessionButton = By.id(String.format("%s%s", driver.PACK, "id/close_button"));
    private By encashmentPopupTitle = By.xpath("//android.widget.FrameLayout[@resource-id='android:id/content']//android.widget.TextView[@resource-id='android:id/title']");
    private By encashmentPopupCancelButton = By.id(String.format("%s:id/cancel_encashment", driver.PACK));
    private By encashmentPopupAcceptButton = By.id(String.format("%s:id/accept_encashment", driver.PACK));
    private By encashmentPopupEdit = By.id(String.format("%s:id/encashment_edit", driver.PACK));

    public ExitPage(LiteboxMobileDriver driver) {
        super(driver);
    }

    public AnyCashBoxPage exitSession() {
        Log.info("Закрываем смену");
        el(closeSessionButton).click();
        encashment(true);
        return new AnyCashBoxPage(driver);
    }

    public AnyCashBoxPage cancelExit() {
        Log.info("Отменяем закрытие смены");
        el(cancelCloseSessionButton).click();
        return new AnyCashBoxPage(driver);
    }

    public ExitPage encashment(boolean encashment) {
        if (waitIsVisibleElement(encashmentPopupTitle, 2, "Инкасация")) {
            if (el(encashmentPopupTitle).getText().equals("Инкассация")) {
                if (encashment) {
                    String summ = el(encashmentPopupEdit).getText();
                    summ = summ.substring(summ.indexOf(" ")+1, summ.indexOf("."));
                    el(encashmentPopupEdit).sendKeys(summ);
                    el(encashmentPopupAcceptButton).click();



                } else {
                    el(encashmentPopupCancelButton).click();
                }
            }
        }

        return this;
    }
}
