package basic;

import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CashboxMainPage;
import pages.LoginPage;
import pages.OpenSessionPage;

public class BaseTest {
    protected static final LiteboxMobileDriver driver;
    private static final String LITEBOX_LOGIN = System.getProperty("litebox.login");
    private static final String LITEBOX_PASSWORD = System.getProperty("litebox.password");
    private WebDriverWait wait;

    private By autorization = By.id(String.format("%s%s", driver.PACK, ":id/authorization_buttons"));
    private By favorite = By.id(String.format("%s%s", driver.PACK, ":id/mainToolbarTitle"));


    static {
        if (null == LITEBOX_LOGIN) {
            Assert.fail("Parameter \"litebox.login\" is not specified. Please use JVM option -Dlitebox.login=\"<your_login>\"");
        }
        if (null == LITEBOX_PASSWORD) {
            Assert.fail("Parameter \"litebox.password\" is not specified. Please use JVM option -Dlitebox.password=\"<your_password>\"");
        }
        driver = new LiteboxMobileDriver();
    }

    @Rule
    public TestWatcher testWatcher = new TestWatcher() {
        @Override
        protected void starting(Description description) {
            Log.info("Начинаем " + description.getDisplayName());
            super.starting(description);
        }

        @Override
        protected void failed(Throwable e, Description description) {
            driver.takeScreenshot();
            Log.warning("Error! " + description.getDisplayName() + " failed: \n" + Utils.stackTraceToString(e));
            super.failed(e, description);
        }

        @Override
        protected void skipped(AssumptionViolatedException e, Description description) {
            Log.info("Пропущено: " + e.getMessage());
            super.skipped(e, description);
        }

        @Override
        protected void finished(Description description) {
            Log.info("Закончили " + description.getDisplayName() + "\n");
            super.finished(description);
        }
    };

    @BeforeClass
    public static void beforeClass() {
    }

    @AfterClass
    public static void quitDriver() {
        Log.info("Усё, закрываем окошко...");
    }


    protected CashboxMainPage cashbox() {
        if (!waitLoadActivityName("litebox", 2)) {
            Log.info("Приложение не запущено");
            driver.getWebDriver().launchApp();
        }
        if (waitLoadActivityName("AuthorizationActivity_", 5)) {
            Log.info("Уже в окне Авторизации");
            return new LoginPage(driver).login(LITEBOX_LOGIN, LITEBOX_PASSWORD).onlyOpenSession();
        } else if (waitLoadActivityName("FavoritesActivity", 3)) {
            Log.info("Уже в окне Избранное");
            return new CashboxMainPage(driver);
        } else {
            driver.getWebDriver().closeApp();
            return cashbox();
        }
    }

    protected OpenSessionPage cashboxAndDepositCash() {
        if (!waitLoadActivityName("litebox", 2)) {
            Log.info("Приложение не запущено");
            driver.getWebDriver().launchApp();
        }
        if (waitLoadActivityName("AuthorizationActivity_", 5)) {
            Log.info("Уже в окне Авторизации");
            return new LoginPage(driver).login(LITEBOX_LOGIN, LITEBOX_PASSWORD);
        } else {
            new CashboxMainPage(driver).exit();
        }
        return cashboxAndDepositCash();
    }


    protected boolean waitLoadActivityName(String activityName, int seconds) {

        WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), seconds);
        long beginTime = System.currentTimeMillis();
        try {
            wait.until(d -> driver.currentActivity().contains(activityName));
            Log.info(String.format(" ...ожидал появления активити %s: %s мс", activityName, (System.currentTimeMillis() - beginTime)));
            return true;
        } catch (TimeoutException ex) {
            Log.info(String.format("В течении %d секунд не появилась активити %s", activityName));
            return false;
        }
    }

        /*
        activity = driver.currentActivity();
        activity = driver.currentActivity();
        activity = driver.currentActivity();
        wait = new WebDriverWait(driver.getWebDriver(), 5);
        long beginTime = System.currentTimeMillis();
        try {
            wait.until(d -> d.findElement(autorization).isDisplayed());
            Log.info(String.format(" ...waited %s ms", (System.currentTimeMillis() - beginTime)));
            return new LoginPage(driver).login(LITEBOX_LOGIN, LITEBOX_PASSWORD);
        } catch (TimeoutException ex) {
            Log.warning(String.format("Уже 5 секунд жду появления элемента %s, нехорошо...\nFrom Exception: %s", autorization.toString(), ex.getMessage()));
            beginTime = System.currentTimeMillis();
            try {
                wait.until(d -> d.findElement(favorite).getText().equals("Избранное"));
                Log.info(String.format(" ...waited %s ms", (System.currentTimeMillis() - beginTime)));
                return new CashboxMainPage(driver);
            } catch (TimeoutException e) {
                Assert.fail(String.format("Уже 10 секунд жду входа в кассу..\nFrom Exception: %s", e.getMessage()));
                return new CashboxMainPage(driver);
            }
        }*/
}
