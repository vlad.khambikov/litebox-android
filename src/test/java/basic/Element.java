package basic;

import io.appium.java_client.MobileElement;
import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Element {
    private MobileElement mobElement;
    private LiteboxMobileDriver driver;

    public Element(MobileElement mobElement, LiteboxMobileDriver driver) {
        this.mobElement = mobElement;
        this.driver = driver;
    }

    public void click() {
        Log.info(String.format(" ...clicking at \"%s\" %s", mobElement.getText(), mobElement.toString()));
        try {
            mobElement.click();
        } catch (Exception e) {
            driver.takeScreenshot();
            Assert.fail(e.getMessage());
        }
        driver.waitUnveil();
    }

    public void sendKeys(CharSequence s) {
        try {
            Log.info(String.format(" ...sending \"%s\" to %s", s, mobElement.toString()));
            mobElement.sendKeys(s);
        } catch (Exception e) {
            driver.takeScreenshot();
            Assert.fail(String.format("Не могу послать строку %s в элемент %s\nFrom Exception: %s", s, mobElement.toString(), e
                    .getMessage()));
        }
        if (driver.getWebDriver().isKeyboardShown()) {
            driver.getWebDriver().hideKeyboard();
        }
    }

    public void sendLogin(String text) {
        if (text.contains(".")) {
            text.replaceAll(".", ".\b");
        }
        sendKeys(text);
    }

    public void clear() {
        mobElement.clear();
    }

    public String getText() {
        String text = null;
        try {
            text = mobElement.getText();
        } catch (NoSuchElementException e) {
            driver.takeScreenshot();
            Assert.fail(String.format("Не могу получить текст элемента %s\nFrom Exception: %s", mobElement.toString(), e
                    .getMessage()));
        }
        return text;
    }

    public boolean isVisibleElementInTime() {
        try {
            return mobElement.isDisplayed();
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isVisibleElementInTime(int second) {
        WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), second);
        try {
            wait.until(d -> mobElement.isDisplayed());
            return true;
        } catch (TimeoutException ex) {
            return false;
        }
    }

    public boolean isSelected() {
        Log.info(String.format(" ...element %s isSelected?", mobElement.toString()));
        return mobElement.isSelected();
    }

    public Point getLocation() {
        return mobElement.getLocation();
    }

    public Dimension getSize() {
        return mobElement.getSize();
    }

    public int getXLeft() {
        return mobElement.getLocation().getX();
    }

    public int getXRight() {
        return mobElement.getLocation().getX() + mobElement.getSize().width;
    }

    public int getYUp() {
        return mobElement.getLocation().getY();
    }

    public int getYDown() {
        return mobElement.getLocation().getY() + mobElement.getSize().height;
    }

    public int getYMiddle() {
        return mobElement.getLocation().getY() + mobElement.getSize().height / 2;
    }

    public String getAttribute(String text) {
        return mobElement.getAttribute(text);
    }

    public boolean waitingForAppearanceOfAttributeWithValue(String attribute, String value, int seconds) {

        WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), seconds);
        long beginTime = System.currentTimeMillis();
        try {
            wait.until(d -> mobElement.getAttribute(attribute).contains(value));
            Log.info(String.format(" ...ожидал появления у элемента %s атрибута %s со значением %s: %s мс", mobElement.toString(), attribute, value, (System.currentTimeMillis() - beginTime)));
            return true;
        } catch (TimeoutException ex) {
            Log.info(String.format("У элемента %s в течении %d секунд не появился атрибут %s со значением %s", mobElement.toString(), seconds, attribute, value));
            return false;
        }
    }
}
