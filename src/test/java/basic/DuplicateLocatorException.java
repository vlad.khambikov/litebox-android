package basic;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

import java.util.List;

public class DuplicateLocatorException extends RuntimeException {
    public DuplicateLocatorException(LiteboxMobileDriver driver, By by, List<MobileElement> found) {
        super(String.format("Duplicate (%d) locators found by %s: ", found.size(), by.toString()));
        driver.takeScreenshot();
    }
}
