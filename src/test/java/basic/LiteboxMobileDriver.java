package basic;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class LiteboxMobileDriver {
    private static final int WAIT_TIMEOUT_IN_SECONDS = 20;
    private static final int LONG_WAIT_TIMEOUT_IN_SECONDS = 120;
    private static final By progerss = By.id("ru.android.liteboxdevelop:id/progressWheel");
    private static final By message = By.id("ru.android.liteboxdevelop:id/message");
    private static List<String> ALLPHONE = new ArrayList<>() {{
        add ("F5122");
    }};

    private static List<String> ALLTABLE = new ArrayList<>() {{
        add("P00A");
    }};

    private AndroidDriver driver;
    private WebDriverWait wait;
    private WebDriverWait longWait;
    public static String PACK;



    public LiteboxMobileDriver(){
        URL SERVER_URL = null;
        try {
            SERVER_URL = new URL("http://127.0.0.1:4723/wd/hub");
        } catch (MalformedURLException e) {
            Assert.fail(e.getMessage());
        }
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "DEVICE");
        //capabilities.setCapability("appPackage", "ru.android.litebox");
        capabilities.setCapability(MobileCapabilityType.APP, "C:/liteboxTest/litebox.apk");
        capabilities.setCapability("unicodeKeyboard", true);
        capabilities.setCapability("resetKeyboard", true);
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 99999);

        driver = new AndroidDriver(SERVER_URL, capabilities);
        Runtime.getRuntime().addShutdownHook(new Thread(driver::quit));
        wait = new WebDriverWait(driver, WAIT_TIMEOUT_IN_SECONDS);
        wait.ignoring(StaleElementReferenceException.class);
        longWait = new WebDriverWait(driver, LONG_WAIT_TIMEOUT_IN_SECONDS);
        longWait.ignoring(StaleElementReferenceException.class);
        PACK = driver.getCurrentPackage();
    }

    public void waitUnveil() {
        Log.info(" ...checking for div-block or pop-up");
        if (!driver.findElements(progerss).isEmpty()) {
            Log.info(String.format(" ...waiting for the disappearance of the load "));
            waitUntilInvisibleIfExists(progerss);
        }
        if (!driver.findElements(message).isEmpty()) {
            Log.info(" ...pop-up-message found (%s), waiting for its disappearance");
            waitUntilInvisibleIfExists(message);
        }
    }

    public void waitUntilVisible(By by) {
        Log.info(" ...waiting for " + by.toString());
        long beginTime = System.currentTimeMillis();
        try {
            wait.until(d -> d.findElement(by).isDisplayed());
        } catch (TimeoutException e1) {
            takeScreenshot();
            Log.warning(String.format("Уже %d секунд жду появления элемента %s, нехорошо...\nFrom Exception: %s", WAIT_TIMEOUT_IN_SECONDS, by
                    .toString(), e1.getMessage()));
            try {
                longWait.until(d -> d.findElement(by).isDisplayed());
            } catch (TimeoutException e2) {
                takeScreenshot();
                Assert.fail(String.format("Так и не дождался за %d секунд появления элемента %s\nFrom Exception: %s", LONG_WAIT_TIMEOUT_IN_SECONDS + WAIT_TIMEOUT_IN_SECONDS, by
                        .toString(), e2.getMessage()));
            }
        }
        Log.info(" ...waited " + (System.currentTimeMillis() - beginTime) + " ms");
    }

    public void waitUntilInvisibleIfExists(By by) {
        Log.info(" ...waiting for disappearance of " + by.toString());
        long beginTime = System.currentTimeMillis();
        try {
            wait.until(d -> (d.findElements(by).isEmpty() || !d.findElement(by).isDisplayed()));
        } catch (TimeoutException e1) {
            takeScreenshot();
            Log.warning(String.format("Уже %d секунд жду, пока пропадёт элемент %s, нехорошо...\nFrom Exception: %s", WAIT_TIMEOUT_IN_SECONDS, by
                    .toString(), e1.getMessage()));
            try {
                longWait.until(d -> (d.findElements(by).isEmpty() || !d.findElement(by).isDisplayed()));
            } catch (TimeoutException e2) {
                takeScreenshot();
                Assert.fail(String.format("Так и не дождался за %d секунд, пока пропадёт элемент %s\nFrom Exception: %s", LONG_WAIT_TIMEOUT_IN_SECONDS + WAIT_TIMEOUT_IN_SECONDS, by
                        .toString(), e2.getMessage()));
            }

        }
        Log.info(" ...waited " + (System.currentTimeMillis() - beginTime) + " ms");
    }

    public void takeScreenshot() {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String shotName = "/tmp/shot" + Utils.random.nextInt(1000000) + ".png";
        try {
            FileUtils.copyFile(scrFile, new File(shotName));
            Log.info("Снят скриншот " + shotName);
        } catch (IOException e) {
            Log.warning("Не удалось сохранить скриншот " + shotName);
            Log.info(Utils.stackTraceToString(e));
        }
    }

    public Element find(By by) {
        List<MobileElement> found = driver.findElements(by);
        switch (found.size()) {
            case 0:
                return null;
            case 1:
                return new Element(found.get(0), this);
            default:
                throw new DuplicateLocatorException(this, by, found);
        }
    }

    public MobileElement findElement(By by) {
        Log.info("Прямой вызов findElement - зачем? By = " + by.toString());
        return (MobileElement) driver.findElement(by);
    }

    public List<MobileElement> findElements(By by) {
        List<MobileElement> elements = driver.findElements(by);
        Log.info(" ...findElements нашёл " + elements.size() + " эл. " + by.toString());
        return elements;
    }


    public WebDriver.Options manage() {
        return driver.manage();
    }



    public WebDriver.TargetLocator switchTo() {
        return driver.switchTo();
    }

    public void close() {
        driver.close();
    }

    public void get(String url) {
        driver.get(url);
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public <V> V waitUntilCondition(int seconds, Function<? super WebDriver, V> isTrue) {
        return (new WebDriverWait(driver, seconds)).until(isTrue);
    }

    public AndroidDriver getWebDriver(){
        return driver;
    }

    public ScreenOrientation getOrientation() {
        return driver.getOrientation();
    }

    public Map<String, Object> getSetting(){
        return driver.getSettings();
    }

    public Map<String, Object> getSessionDetails(){
        return driver.getSessionDetails();
    }

    public String getDeviceModel(){
        return (String) driver.getSessionDetails().get("deviceModel");
    }

    public static List<String> GETALLTABLE(){
        return ALLTABLE;
    }

    public String  currentActivity(){
        return this.driver.currentActivity();
    }

    public String getContext(){
        return this.driver.getContext();
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

}
