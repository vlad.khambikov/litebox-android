package basic;

import org.junit.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Log {
    private static final String LOG_NAME = "mobile.bots";
    private static final Logger logger = Logger.getLogger(LOG_NAME);
    private static final String CONFIG   = "src/test/resources/logging.properties";

    static {
        try {
            InputStream is =  new FileInputStream(new File(CONFIG));
            LogManager.getLogManager().readConfiguration(is);
        } catch (Exception e) {
            Assert.fail(String.format("Cannot read logging properties! \n %s", e.getMessage()));
        }
    }

    public static void debug(String s) {
        logger.fine(s);
    }

    public static void info(String s) {
        logger.info(s);
    }

    public static void warning(String s) {
        logger.warning(s);
    }
}