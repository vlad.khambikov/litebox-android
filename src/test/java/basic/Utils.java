package basic;

import java.util.Random;

public class Utils {
    public static final Random random = new Random();


    public static String stackTraceToString(Throwable ex) {
        StringBuilder result = new StringBuilder("\n-----\n").append(ex.toString()).append("\n-----\n");
        StackTraceElement[] trace = ex.getStackTrace();
        for (StackTraceElement aTrace : trace) {
            result.append("\tat ").append(aTrace.toString()).append("\n");
        }
        return result.toString();
    }

    public static String escapeXpath(String original) {
        if (original.contains("'")) {
            return "concat('" + original.replace("'", "',\"'\",'") + "')";
        } else {
            return "'" + original + "'";
        }
    }

    public static float round(float number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        float tmp = number * pow;
        return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }
}
