import basic.BaseTest;
import basic.Log;
import org.junit.*;
import pages.BaskePage;
import pages.OpenSessionPage;
import pages.SettingPage;

import static basic.Utils.random;


public class SmokeAndroidTest extends BaseTest {

    @BeforeClass
    public static void beforeTest() {
        Log.info("Начинаем тесты кассы");
    }

    @After
    public void after() {
        Log.info("Закончили тестирование кассы");
    }

    @Ignore
    @Test
    public void testSelectAnySetting() {
        Log.info("Тест выбора различных настроек");
        SettingPage page = cashbox().openMainMenu().setting();
        for (int i = 0; i < 10; i++) {
            switch (random.nextInt(5)) {
                case 0:
                    page.gotoExchange();
                    break;
                case 1:
                    page.gotoEGAIS();
                    break;
                case 2:
                    page.gotoEquipment();
                    break;
                case 3:
                    page.gotoCommon();
                    break;
                case 4:
                    page.gotoNotice();
                    break;
            }
        }
        page.exit();
    }

    @Ignore
    @Test
    public void testFreeSale() {
        Log.info("Тест свободной продажи");
        BaskePage page = cashbox().openMainMenu().setting().gotoExchange().completeExchange().gotoFreeSale().enterAnyNumber("258.36e").gotoFreeSale().enterAnyNumber("35.2e").gotoBaskePage();
        float total = Math.round(page.getTotal() / 100) * 100;
        page.pay("Cash", total, true, 20);
        page.exit();
    }

    @Test
    public void testCashDeposit(){
        Log.info("Тест внесения денег в кассу");
        OpenSessionPage page = cashboxAndDepositCash();
        String cashBefore = page.getCashOfHand();
        page.depositMoneyInCash("Служебный внос", "Хозяина","500");
        String cashAfter = page.getCashOfHand();
        Assert.assertFalse("После внесения денег значения поля \"Наличные в кассе\" не изменились", cashAfter.equals(cashBefore));
        Log.info("Деньги внесены в кассу");
    }

}
